package io.qameta.example;

import io.qameta.allure.Allure;
import io.qameta.allure.Link;
import io.qameta.allure.Links;
import org.junit.jupiter.api.Test;

/**
 * Here are examples of how you can make the links in Allure Report more readable.
 * See a screenshot here: ###allure-report-documentation##/static_link
 */
public class AllureLinkTest {

    private static final String LINK_TITLE = "GitHub";
    private static final String LINK_URL = "https://github.com";

    /**
     * The '@Link' annotation is used here, it allows you to assign a single link to a test and also makes it possible
     * to hide them for more readable titles.
     */
    @Test
    @Link(name = LINK_TITLE, url = "https://github.com")
    public void staticLinkTest() {
    }

    /**
     * A special case of point 1, which shows how you can set several static links for one test
     * Pros: Very simple and readable way
     * Cons: Links cannot fetch values from code
     */
    @Test
    @Links({
            @Link(name = "First static link", url = LINK_URL),
            @Link(name = "Second static link", url = "https://github.com")
    })
    public void staticLinksTest() {
    }

    /**
     * 'Allure.link' is a very handy tool, it allows you to quickly access the data obtained during the test run.
     * Pros: The actuality of the data obtained
     */
    @Test
    public void dynamicLinkTest() {
        Allure.link(LINK_TITLE, LINK_URL);
        Allure.link("Second dynamic link", "https://github.com");
    }

}