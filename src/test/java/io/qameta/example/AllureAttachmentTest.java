package io.qameta.example;

import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.junit.jupiter.api.Test;
import java.nio.charset.StandardCharsets;

/**
 * Attachment allows you to pin a file to a test result
 * Here are examples of how you can set up attachments in an allure report
 * See a screenshot here: ###allure-report-documentation##/dynamic_attachment
 */
public class AllureAttachmentTest {

    /**
     * Allows you to provide attachments using the Allure.attachment method
     * Pros: The code is more readable
     * Cons: More restrictions on input data
     */
    @Test
    public void dynamicAttachmentTest() {
        Allure.attachment("Dynamic attachment", "attachment content");
    }

    /**
     * Allows you to specify the type of attachment and its content using method
     * Pros: You can create custom rules for attachments
     * Cons: More complex code
     */
    @Test
    public void annotatedAttachmentTest() {
        Allure.step("Here goes the step with annotated attachment", () -> {
            textAttachment("Annotated", "hello, world!");
        });
    }

    /**
     * Method provided by '@Attachment' annotation and describes which files you want to attach.
     */
    @Attachment(value = "Annotated attachment [{name}]", type = "text/plain", fileExtension = ".txt")
    public byte[] textAttachment(String name, String content) {
        return content.getBytes(StandardCharsets.UTF_8);
    }

}