package io.qameta.example;

import io.qameta.allure.Allure;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.LabelAnnotation;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Test;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Labels in the Allure report allow you to sort the tests in the tree by its behaviors and environments
 * See a screenshot here: ###allure-report-documentation##/common_dynamic_label
 */
public class AllureLabelsTest {

    /**
     * Allows you to create a label based on a common basic annotation
     * Pros: Hard to get wrong, all labels are placed directly above the test.
     * Cons: Unable to change values from test results
     */
    @Test
    @Epic("static epic")
    @Feature("static feature")
    @Story("static story")
    @Owner("static owner")
    public void commonStaticLabelTest() {
    }

    /**
     * Allows you to create base labels from code using internal Allure shortcuts.
     * Pros: Allows to change the value of a label by taking it from the test results
     * Cons: More control over code quality is needed, any mistake can distort the test results tree.
     */
    @Test
    public void commonDynamicLabelTest() {
        Allure.epic("dynamic epic");
        Allure.feature("dynamic feature");
        Allure.story("dynamic story");
        Allure.suite("dynamic suite");
    }

    /**
     * Allows you to create your own labels using the '@CustomLabel' annotation and an interface that describes
     * your rules for that label.
     * Pros: Own label, with custom name and value. Placed directly above the test.
     * Cons: Need to create an additional interface in the code. Unable to change values from test results
     */
    @Test
    @CustomLabel("static value")
    public void customStaticLabelTest() {
    }

    /**
     * Allows you to create your own label using the '@CustomLabel' annotation and a method that describes your
     * rules for that label.
     * Pros: Own label, with a custom name and value. Allows you to change the value of the label by taking it from
     * a test run.
     * Cons: Need more control over code quality, any mistake can distort test results tree.
     */
    @Test
    public void customDynamicLabelTest() {
        Allure.label("custom", "dynamic value");
    }

    /**
     * Public interface describing '@CustomLabel' annotation
     */
    @Documented
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.METHOD, ElementType.TYPE})
    @LabelAnnotation(name = "custom")
    public @interface CustomLabel {
        String value();
    }

}