package io.qameta.example;

import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import org.junit.jupiter.api.Test;

/**
 * A test description helps you quickly understand its purpose without going into unnecessary detail.
 * Here are examples of how you can set a description in an allure report
 * See a screenshot here: ###allure-report-documentation##/javadoc_description
 */
public class AllureDescriptionTest {

    /**
     * The basic annotation '@Description' is used
     * Pros: Very simple method, hard to get wrong
     * Cons: If you change the script, you will have to manually rewrite it.
     */
    @Test
    @Description("Static description")
    public void annotationDescriptionTest() {
    }

    /**
     * Uses a basic '@Description' annotation and a javaDoc method that allows you to pass text commented out over the test
     * Pros: Very handy if you're used to keeping documentation in code.
     * Cons: The description is not strongly tied to the test, there is a chance to make a mistake.
     */
    @Test
    @Description(useJavaDoc = true)
    public void javadocDescriptionTest() {
    }

    /**
     * Uses Allure.description method, allows you to adjust to the test scenario and keep description up to date
     * Pros: Description is always up to date
     * Cons: Requires more control
     */
    @Test
    public void dynamicDescriptionTest() {
        Allure.description("Dynamic description");
    }

}